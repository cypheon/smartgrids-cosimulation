#!/usr/bin/env python3

import matplotlib.pyplot as plt
import mosaik
import mosaik.scenario
import mosaik.util as util
import numpy as np

from helpers.plotting import fill_area, format_time_axis, plot_series

PRICE_PER_KWH_GRID = 35
EARNINGS_PER_KWH = 8

BATTERY_LEVEL_START = 30  # percent
BATTERY_CAPACITY = 82  # kWh

def setUp(world: mosaik.World, monitor: mosaik.scenario.Entity, smart: bool):
    ngspicesim = world.start('NgspiceSim', eid_prefix='SM_')
    appliancesim = world.start('ApplianceSim')
    controllersim = world.start('ControllerSim')
    producersim = world.start('ProducerSim')
    prosumersim = world.start('ProsumerSim')
    eventsourcesim = world.start('EventSourceSim')

    if smart:
        behaviour = 'wm_smart'
    else:
        behaviour = 'wm_simple'

    washing_machine = appliancesim.Appliance(
      idle_power=3, active_power=1000,
      program_duration=5*3600,
      behaviour=behaviour
    )

    fridge = appliancesim.ThermalAppliance(
        idle_power=5, active_power=250,
        loss_idle=0.05612568731830647,  # 50% in 12 hours
        loss_active=0.8,  # 80% in 1 hour
        heat_factor=-0.001111111111,  # -4K in 1 hour
        target_temp=4,
        behaviour=behaviour
    )

    water_heater = appliancesim.ThermalAppliance(
        idle_power=5, active_power=500,
        loss_idle=0.05612568731830647,  # 50% in 12 hours
        loss_active=0.9,  # 90% in 1 hour
        heat_factor=20/3600,  # +20K in 1 hour
        target_temp=65,
        behaviour=behaviour
    )

    pv_simple = producersim.LookupProducer(
        max_power = 6000,
        # start_timestamp=1145656800  # 2006-04-22
        start_timestamp=1150322400  # 2006-06-15
    )
    prosumer_simple = prosumersim.Prosumer(
      battery_charge = BATTERY_LEVEL_START,
      battery_power = 11000,
      battery_capacity = BATTERY_CAPACITY,
      behaviour=behaviour
    )

    smart_meter = ngspicesim.SmartMeter()
    smart_controller = controllersim.SmartController()

    # trigger event at 07:30:00
    es1 = eventsourcesim.EventSource(time_seconds=7.5 * 3600)

    util.connect_many_to_one(world, [
        washing_machine,
        fridge,
        water_heater,
        prosumer_simple,
    ], smart_meter, 'power_usage')

    util.connect_many_to_one(world, [
        washing_machine,
        fridge,
        water_heater,
        prosumer_simple,
    ], smart_controller , ('opt_power_modes', 'extra_power_requests'))

    util.connect_many_to_one(world, [
        pv_simple,
        prosumer_simple,
    ], smart_meter, 'power_output')

    world.connect(es1, washing_machine, ('event', 'ready'))

    world.connect(smart_meter, smart_controller, 'power_grid_w')

    world.connect(smart_controller, washing_machine, 'extra_power_allocation',
                  time_shifted=True, initial_data={'extra_power_allocation':{}})

    world.connect(smart_controller, fridge, 'extra_power_allocation',
                  time_shifted=True, initial_data={'extra_power_allocation':{}})

    world.connect(smart_controller, water_heater, 'extra_power_allocation',
                  time_shifted=True, initial_data={'extra_power_allocation':{}})

    world.connect(smart_controller, prosumer_simple, 'extra_power_allocation',
                  time_shifted=True, initial_data={'extra_power_allocation':{}})

    world.connect(smart_meter, monitor, 'power_grid_w', 'power_total_w')
    util.connect_many_to_one(world, [
        washing_machine,
        fridge,
        water_heater,
    ], monitor, 'power_usage')
    world.connect(pv_simple, monitor, 'power_output')
    world.connect(fridge, monitor, 'inner_temp', 'power_usage')
    world.connect(water_heater, monitor, 'inner_temp', 'power_usage')
    world.connect(prosumer_simple, monitor, 'battery_level', 'power_usage', 'power_output')


def plot(np_data):
    fig, (ax1, ax3, ax2) = plt.subplots(3, sharex=True)
    format_time_axis(ax1)
    format_time_axis(ax2)
    format_time_axis(ax3)

    plot_series(ax1, np_data["ApplianceSim-0.App_0.power_usage"], label="Verbrauch Waschtrockner")
    plot_series(ax1, np_data["ProducerSim-0.Prod_0.power_output"], label="PV erzeugte Leistung")

    ax1.legend(loc='upper left')
    ax1.grid()
    ax1.set_ylabel("Leistung (W)")

    pwgrid = np_data['NgspiceSim-0.SM_0.power_grid_w']
    plot_series(ax2, pwgrid, color="black", label="Netto Leistungsbilanz")
    fill_area(ax2, pwgrid, 'pos', alpha=0.4, label="bezogene Energie")
    fill_area(ax2, pwgrid, 'neg', alpha=0.4, label="eingespeiste Energie")
    ax2.legend(loc='upper left')
    ax2.grid()
    ax2.set_ylabel("Leistung (W)")

    ax2.set_xlabel("Uhrzeit")

    plot_series(ax3, np_data["ApplianceSim-0.App_1.inner_temp"], color="black", label="Temp. Kühlschrank")
    # plot_series(ax3, np_data["ApplianceSim-0.App_2.inner_temp"], color="red", label="Temp. Warmwasserspeicher")
    ax3.legend(loc='upper left')
    ax3.grid()
    ax3.set_ylabel("Temperatur (°C)")

    return fig


def summary(np_data):
    pwgrid = np_data['NgspiceSim-0.SM_0.power_grid_w']

    # energy taken from the grid
    pwgrid_used = np.clip(pwgrid[:, 1], 0, None)
    total_used_j = np.trapz(pwgrid_used, pwgrid[:, 0])
    used_kwh = total_used_j / 3.6e6

    # energy fed into the grid
    pwgrid_fed = np.clip(pwgrid[:, 1], None, 0)
    total_fed_j = np.trapz(pwgrid_fed, pwgrid[:, 0])
    fed_kwh = total_fed_j / 3.6e6

    cost_used = used_kwh * PRICE_PER_KWH_GRID
    comp_fed = fed_kwh * EARNINGS_PER_KWH

    total_energy = used_kwh + fed_kwh
    total_cost = cost_used + comp_fed

    bat_charge_level_end = np_data['ProsumerSim-0.Pros_0.battery_level'][-1,1]
    bat_charge_level_delta = bat_charge_level_end - (BATTERY_LEVEL_START / 100)
    bat_energy_worth = bat_charge_level_delta * BATTERY_CAPACITY * PRICE_PER_KWH_GRID

    return f"""
Energie aus Netz bezogen:    {used_kwh:9.3f} kWh
Kosten für bezogene Energie:                  {cost_used:4.0f} ct
Energie in Netz eingespeist: {fed_kwh:9.3f} kWh
Einspeisevergütung:                           {comp_fed:4.0f} ct
=====================================================
Gesamt:                      {total_energy:9.3f} kWh
Gesamtkosten:                                 {total_cost:4.0f} ct

Batterie-Ladezustend Delta:   {bat_charge_level_delta*100:.1f}%
"Wert" der Batterie-Ladung:   {bat_energy_worth:.0f} ct
"""
