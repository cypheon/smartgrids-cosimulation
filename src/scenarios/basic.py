#!/usr/bin/env python3

import matplotlib.pyplot as plt
import mosaik
import mosaik.scenario
import mosaik.util as util
import numpy as np

from helpers.plotting import fill_area, format_time_axis, plot_series

# Appliance parameters
idle = 3
active = 2000
duration = 5
start = 6
# Producer parameters
maximum_power_output = 2600
clouded_start = 14
clouded_duration = 2

def setUp(world: mosaik.World, monitor: mosaik.scenario.Entity, smart: bool):
    ngspicesim = world.start('NgspiceSim', eid_prefix='SM_')
    appliancesim = world.start('ApplianceSim')
    controllersim = world.start('ControllerSim')
    producersim = world.start('ProducerSim')
    eventsourcesim = world.start('EventSourceSim')

    if smart:
        behaviour = 'wm_smart'
    else:
        behaviour = 'wm_simple'

    washing_machine = appliancesim.Appliance(
      idle_power=idle, active_power=active,
      program_duration=duration*3600,
      behaviour=behaviour
    )
    pv_simple = producersim.ModelProducer(
      low_power = 120,
      average_power = 450,
      max_power = maximum_power_output,
      clouded_start = clouded_start,
      clouded_duration = clouded_duration
    )

    smart_meter = ngspicesim.SmartMeter()
    smart_controller = controllersim.SmartController()

    # trigger event at 06:00:00
    es1 = eventsourcesim.EventSource(time_seconds=start * 3600)

    util.connect_many_to_one(world, [
        washing_machine,
    ], smart_meter, 'power_usage')

    util.connect_many_to_one(world, [
        washing_machine,
    ], smart_controller , ('opt_power_modes', 'extra_power_requests'))

    util.connect_many_to_one(world, [
        pv_simple,
    ], smart_meter, 'power_output')

    world.connect(es1, washing_machine, ('event', 'ready'))

    world.connect(smart_meter, smart_controller, 'power_grid_w')
    world.connect(smart_controller, washing_machine, 'extra_power_allocation',
                  time_shifted=True, initial_data={'extra_power_allocation':{}})

    world.connect(smart_meter, monitor, 'power_grid_w', 'power_total_w')
    world.connect(washing_machine, monitor, 'power_usage')
    world.connect(pv_simple, monitor, 'power_output')


def plot(np_data):
    fig, (ax1, ax2) = plt.subplots(2, sharex=True, figsize=(10, 6))
    format_time_axis(ax1)
    format_time_axis(ax2)

    plot_series(ax1, np_data["ApplianceSim-0.App_0.power_usage"], label="Verbrauch Waschtrockner")
    plot_series(ax1, np_data["ProducerSim-0.Prod_0.power_output"], label="PV erzeugte Leistung")

    ax1.legend(loc='upper left')
    ax1.grid()
    ax1.set_ylabel("Leistung (W)")

    pwgrid = np_data['NgspiceSim-0.SM_0.power_grid_w']
    plot_series(ax2, pwgrid, color="black", label="Netto Leistungsbilanz")
    fill_area(ax2, pwgrid, 'pos', alpha=0.4, label="bezogene Energie")
    fill_area(ax2, pwgrid, 'neg', alpha=0.4, label="eingespeiste Energie")
    ax2.legend(loc='upper left')
    ax2.grid()
    ax2.set_ylabel("Leistung (W)")

    ax2.set_xlabel("Uhrzeit")

    return fig


def summary(np_data):
    pwgrid = np_data['NgspiceSim-0.SM_0.power_grid_w']

    # energy taken from the grid
    pwgrid_used = np.clip(pwgrid[:, 1], 0, None)
    total_used_j = np.trapz(pwgrid_used, pwgrid[:, 0])
    used_kwh = total_used_j / 3.6e6

    # energy fed into the grid
    pwgrid_fed = np.clip(pwgrid[:, 1], None, 0)
    total_fed_j = np.trapz(pwgrid_fed, pwgrid[:, 0])
    fed_kwh = total_fed_j / 3.6e6

    cost_used = used_kwh * 35
    comp_fed = fed_kwh * 8

    total_energy = used_kwh + fed_kwh
    total_cost = cost_used + comp_fed

    return f"""
Energie aus Netz bezogen:    {used_kwh:9.3f} kWh
Kosten für bezogene Energie:                  {cost_used:4.0f} ct
Energie in Netz eingespeist: {fed_kwh:9.3f} kWh
Einspeisevergütung:                           {comp_fed:4.0f} ct
=====================================================
Gesamt:                      {total_energy:9.3f} kWh
Gesamtkosten:                                 {total_cost:4.0f} ct
"""
