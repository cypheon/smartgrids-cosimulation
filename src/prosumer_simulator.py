import mosaik_api
import prosumer_model
from helpers.mosaik_helper import FullSimulator, SimpleSimulator

META = {
    'type': 'time-based',
    'models': {
        'Prosumer': {
            'public': True,
            'params': ['battery_charge', 'battery_power', 'battery_capacity', 'behaviour'],
            'attrs': [
                'battery_level',
                'extra_power_allocation',
                'opt_power_modes',
                'power_usage',
                'power_output',
            ],
        },
    },
}

@FullSimulator(META, default_eid_prefix='Pros_')
class ProsumerSimulator(SimpleSimulator):
    def __init__(self, full_id, time_resolution, model, **kwargs):
        super().__init__(full_id, time_resolution, model)

        self.time = 0

        self.model_instance = prosumer_model.Prosumer(
            full_id, full_id,
            kwargs['battery_charge'],
            kwargs['battery_power'],
            kwargs['battery_capacity']
        )

    def step(self, time, inputs, max_advance):
        extra_power = self.find_power_budget(inputs.get('extra_power_allocation'))

        self.model_instance.step((time - self.time) * self.time_resolution,
                            extra_power)

        self.time = time
        return time + 1

    def get_data(self, attrs):
        data = {}
        model_power = self.model_instance.calculate_power_usage()
        for attr in attrs:
            if attr not in META['models']['Prosumer']['attrs']:
                raise ValueError(f'Unknown output attribute: {attr}')
            if attr == 'battery_level':
                data[attr] = self.model_instance.battery_charge_level()
            if attr == 'power_usage':
                if model_power > 0:
                    data[attr] = model_power
                else:
                    data[attr] = 0
            if attr == 'power_output':
                if model_power < 0:
                    data[attr] = -model_power
                else:
                    data[attr] = 0
            if attr == 'opt_power_modes':
                data[attr] = self.model_instance.get_optional_extra_power_usage()
        return data


    def find_power_budget(self, attrs) -> float:
        if attrs is None:
            return 0.0

        for src, allocations in attrs.items():
            if self.full_id in allocations:
                return allocations[self.full_id]
        return 0.0

def main():
    return mosaik_api.start_simulation(ProsumerSimulator())

if __name__ == '__main__':
    main()
