import mosaik_api
from helpers.mosaik_helper import FullSimulator, SimpleSimulator

META = {
    'type': 'time-based',
    'models': {
        'EventSource': {
            'public': True,
            'params': ['time_seconds'],
            'attrs': ['event'],
        },
    },
}

@FullSimulator(META, default_eid_prefix='ES_')
class EventSourceSim(SimpleSimulator):
    def __init__(self, full_id, time_resolution, model, **kwargs):
        super().__init__(full_id, time_resolution, model)

        self.event_time = int(kwargs['time_seconds']) // time_resolution

    def step(self, time, inputs, max_advance):
        self.time = time
        return time + 1

    def get_data(self, outputs):
        if self.time == self.event_time:
            return {'event': 1}
        return {}

def main():
    return mosaik_api.start_simulation(ApplianceSimulator())

if __name__ == '__main__':
    main()
