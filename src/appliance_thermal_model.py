from typing import List, Tuple

from appliance_model import Appliance

class ThermalAppliance(Appliance):
    """
    An appliance subclass that models a device that maintains a specified
    target temperature using a heat pump.  When the heat pump is off, the
    temperature slowly approaches the environment temperature.
    """

    def __init__(self, id, name: str, idle_power: float, active_power: float,
                 behaviour: str,
                 loss_idle: float, loss_active: float, heat_factor: float,
                 target_temp: float, env_temp: float = 20):
        """
        the interpretation of "in_use" depends on the type of appliance:
         * fridge->door open
         * water heater->tap running

        heat_factor:
            how much the inner temperature changes (in K/s) when the pump is
            running
        loss_idle:
            how much of the temperature delta is lost per hour (always)
        loss_active:
            how much of the temperature delta is lost during active
            use
        """
        super().__init__(id, name, idle_power, active_power,
                         program_duration_sec=60,  # ignored
                         behaviour=behaviour)
        self.model = 'ThermalAppliance'
        self.target_temp = target_temp

        self.env_temp = env_temp
        self.heat_factor = heat_factor

        # assume steady state at the beginning:
        self.inner_temp = self.target_temp

        # convert temperature delta lost per hour to delta retained per second
        self.alpha_idle = (1.0 - loss_idle) ** (1/3600)
        self.alpha_active = (1.0 - loss_active) ** (1/3600)

        # appliance is in active use by person
        self.in_use = False

        # heat pump running
        self.pump_running = False

    def step(self, timestep_seconds: float, allocated_extra_power: float):
        # physics part, simulate temperature gradient
        temp_delta = self.env_temp - self.inner_temp

        if self.in_use:
            alpha = self.alpha_idle * self.alpha_active
        else:
            alpha = self.alpha_idle

        self.inner_temp = (self.env_temp -
                           temp_delta * alpha ** timestep_seconds)

        self.step_logic(timestep_seconds, allocated_extra_power > 0)

        if self.pump_running:
            self.inner_temp += self.heat_factor * timestep_seconds

    def step_logic(self, timestep_seconds: float, have_extra_power: bool):
        thresh_min, thresh_max = self.get_thresholds(have_extra_power)

        if self.inner_temp > thresh_max:
            self.pump_running = self.is_cooling()
        elif self.inner_temp < thresh_min:
            self.pump_running = not self.is_cooling()
        else:
            # do nothing, keep previous pump status
            pass

    def get_thresholds(self, have_extra_power: bool) -> Tuple[float, float]:
        if self.behaviour == 'wm_smart' and have_extra_power:
            if self.is_cooling():
                bias = -1.0
            else:
                bias = 3
        else:
            bias = 0.0

        thresh_min = self.target_temp - 0.5 + bias
        thresh_max = self.target_temp + 0.5 + bias

        return thresh_min, thresh_max

    def is_active(self) -> bool:
        return self.pump_running

    def is_cooling(self) -> bool:
        """
        Returns if this appliance is a cooling device (True) or a heating
        device (False)
        """
        return self.heat_factor < 0

    def could_run(self) -> bool:
        """
        Check if extra power could be used to cool/heat a bit more
        """
        return True

    def get_optional_extra_power_usage(self) -> List[float]:
        if not self.is_active():
            return [self.active_power - self.idle_power]
        else:
            return [1]
