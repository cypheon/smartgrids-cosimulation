import unittest

from ngspice_model import Model

class ModelTestCase(unittest.TestCase):
    def setUp(self):
        self.m = Model()

    def test_total_power_only_consumers(self):
        self.m.consumers = {
            'computer': 200,
            'microwave': 800,
        }
        result = self.m.simulate()

        self.assertAlmostEqual(result['power_grid_w'], 1000, 1)
        self.assertAlmostEqual(result['power_total_w'], 1000, 1)

    def test_total_power_only_producers(self):
        self.m.producers = {
            'pv': 2000,
        }
        result = self.m.simulate()

        self.assertAlmostEqual(result['power_grid_w'], -2000, 1)
        self.assertAlmostEqual(result['power_total_w'], 0, 1)

    def test_total_power_mixed(self):
        self.m.producers = {
            'pv': 200,
        }
        self.m.consumers = {
            'microwave': 600,
        }
        result = self.m.simulate()

        self.assertAlmostEqual(result['power_grid_w'], 400, 1)
        self.assertAlmostEqual(result['power_total_w'], 600, 1)

    def test_total_power_mixed_surplus(self):
        self.m.producers = {
            'pv': 2000,
        }
        self.m.consumers = {
            'microwave': 600,
        }
        result = self.m.simulate()

        self.assertAlmostEqual(result['power_grid_w'], -1400, 1)
        self.assertAlmostEqual(result['power_total_w'], 600, 1)
