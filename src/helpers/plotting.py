"""
Helper functions assisting with plotting simulation results
"""

import re

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def format_timestep(time_of_day: float, pos):
    hours = (int(time_of_day) // 3600) % 24
    minutes = (int(time_of_day) // 60) % 60
    return f'{hours:02d}:{minutes:02d}'

def gather_results(data):
    all_series = {}

    for sim, sim_data in data.items():
        for series, values in sim_data.items():
            full_name = f'{sim}.{series}'
            try:
                array = np.asarray(sorted([(int(t), v) for t,v in values.items()]), dtype='f8')
            except ValueError as e:
                # ignore data that is not a float array
                print(f'Ignored data series {series}: {e}')
                continue
            all_series[full_name] = array
    return all_series


def show_series(data):
    for name, _ in sorted(gather_results(data).items()):
        print(name)


def fill_area(ax, data, mode=None, **kwargs):
    if mode == 'pos':
        values = np.clip(data[:,1], 0, None)
    elif mode == 'neg':
        values = np.clip(data[:,1], None, 0)
    else:
        values = data[:,1]
    ax.fill_between(data[:,0], values, **kwargs)


def plot_series(ax, data, **kwargs):
    ax.plot(data[:,0], data[:,1], **kwargs)


def format_time_axis(ax):
    ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(3600 * 3))
    ax.xaxis.set_major_formatter(format_timestep)

def plot_glob(data, pattern_left: str, pattern_right: str = '^$',
              exclude_pattern: str = '^$'):
    """
    Plot all results that match the given regex(es), but not the exclude
    pattern.
    """
    r_pattern = re.compile(pattern_right, re.IGNORECASE)
    l_pattern = re.compile(pattern_left, re.IGNORECASE)
    ex_pattern = re.compile(exclude_pattern, re.IGNORECASE)
    fig, ax = plt.subplots(figsize=(10,6))
    ax.set_ylabel('Leistung in W')
    axr = ax.twinx()
    ax.xaxis.set_major_locator(matplotlib.ticker.MultipleLocator(3600 * 3))
    ax.xaxis.set_major_formatter(format_timestep)

    all_series = gather_results(data)
    for full_name, plot_data in all_series.items():
        if ex_pattern.search(full_name) is not None:
            continue
        match_left = l_pattern.search(full_name) is not None
        match_right = r_pattern.search(full_name) is not None
        if match_left or match_right:
            side = ax
            if match_right:
                side = axr
            side.plot(plot_data[:,0], plot_data[:,1], label=full_name)
    ax.legend(loc='upper left', fontsize='small')
    axr.legend(loc='upper right', fontsize='small')
    plt.grid()
    return fig
