"""
Return data based on a lookup in a CSV file
"""

import csv
import datetime

import numpy as np

class CSVLookup:
    def __init__(self, filename: str, scale: float):
        with open(filename, encoding='ascii') as infile:
            raw_rows = csv.reader(infile)
            self.header = raw_rows.__next__()

            timesteps = []
            values = []
            for raw_date, raw_value in raw_rows:
                # Use manual time parsing, because it is almost twice as fast
                # as datetime.strptime():
                m = int(raw_date[0:2])
                d = int(raw_date[3:5])
                y = int(raw_date[6:8])
                H = int(raw_date[9:11])
                M = int(raw_date[12:14])
                date = datetime.datetime(2000+y,m,d,H,M)
                value = float(raw_value) * scale
                timesteps.append(int(date.timestamp()))
                values.append(value)

            self.data = np.array([timesteps, values])


    def lookup(self, dt: int):
        return np.interp(dt, self.data[0], self.data[1])
