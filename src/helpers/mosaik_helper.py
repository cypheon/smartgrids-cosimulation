"""
A small helper class that takes care of the "bookkeeping" for the Mosaik API
regarding generating EntityIDs, and dispatching calls to the individual
`SimpleSimulator`s.
"""

import mosaik_api

class SimpleSimulator:
    def __init__(self, full_id, time_resolution, model, **kwargs):
        self.full_id = full_id
        self.time_resolution = time_resolution
        self.model = model
        self.time = 0

    def step(self, time, inputs, max_advance):
        raise NotImplementedError("create() not implemented")

    def step(self, time, inputs, max_advance):
        raise NotImplementedError("step() not implemented")

    def get_data(self, outputs):
        raise NotImplementedError("get_data() not implemented")

class GenericSimulator(mosaik_api.Simulator):
    def __init__(self, meta, sim_class, default_eid_prefix):
        super().__init__(meta)

        self.sim_class = sim_class
        self.eid_prefix = default_eid_prefix
        self.entities = {}
        self.time = 0

        self.__eid_counter = 0

    def init(self, sid, time_resolution, eid_prefix=None):
        self.sid = sid
        self.time_resolution = time_resolution
        if eid_prefix is not None:
            self.eid_prefix = eid_prefix

        return self.meta

    def create(self, num, model, **kwargs):
        created = []
        for i in range(num):
            fresh_eid = self._next_eid()
            full_id = f'{self.sid}.{fresh_eid}'
            self.entities[fresh_eid] = self.do_create(full_id, model, **kwargs)
            created.append({
                'eid': fresh_eid,
                'type': model,
            })
        return created

    def do_create(self, full_id, model, **kwargs):
        return self.sim_class(full_id, self.time_resolution, model, **kwargs)

    def step(self, time, inputs, max_advance):
        self.time = time
        min_next_step = time + 1
        for eid, sim_instance in self.entities.items():
            attrs = inputs.get(eid, {})
            next_step = sim_instance.step(time, attrs, max_advance)
            if next_step < min_next_step:
                min_next_step = next_step

        return min_next_step

    def get_data(self, outputs):
        data = {}
        data['time'] = self.time
        for eid, attrs in outputs.items():
            sim_instance = self.entities[eid]
            data[eid] = sim_instance.get_data(attrs)
        return data

    def _next_eid(self):
        eid = self.__eid_counter
        self.__eid_counter += 1
        return f'{self.eid_prefix}{eid}'


def FullSimulator(meta, default_eid_prefix='Model_'):
    def f(c):
        def construct():
            return GenericSimulator(meta, c, default_eid_prefix)
        return construct
    return f
