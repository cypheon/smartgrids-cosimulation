import unittest

from helpers.csv_lookup import CSVLookup

# FILENAME = 'data/Actual_48.75_-122.45_2006_DPV_9MW_5_Min.csv'
FILENAME = 'data/test_small.csv'

class CSVLookupTestCase(unittest.TestCase):
    def setUp(self):
        self.csv = CSVLookup(FILENAME, 1 / 9)

    def test_access(self):
        self.assertAlmostEqual(self.csv.lookup(9), 0)


    def test_exact(self):
        # 2006-06-09 13:00
        self.assertAlmostEqual(self.csv.lookup(1149850800), 4.19 / 9)
        # 2006-06-09 13:45
        self.assertAlmostEqual(self.csv.lookup(1149853500), 4.23 / 9)

    def test_interpolate(self):
        # 2006-06-09 13:02:30
        expected = (4.19 + 3.87) / 2
        self.assertAlmostEqual(self.csv.lookup(1149850800 + 150), expected / 9)
