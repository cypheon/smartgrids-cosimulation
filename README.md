Smart grids are characterized by intelligent communication between individual
grid participants. An analysis of current flows is dependent on considering the
behavior of the end devices. Therefore, to efficiently simulate the behavior of
complex smart-grids, different ’single-domain’ simulation tools have to be
combined in the concept of a co-simulation. This challenge leads to a central
question: how can existing open source software be combined to provide an
easily understandable and adaptable simulation framework?

This work focuses on the easy understanding of the behavior of the power grid
and a suitable visualization. As a result, a prototypical simulation framework
— based on open source tools like Python, NGSPICE, NumPy, matplotlib and the
Mosaik framework — will be created to clearly present essential concepts.
